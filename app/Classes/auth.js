import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';


export const USER_KEY = 'auth-key';
export const USER = 'user-key';


export const console_log = (message, isProduction = false) => {
    if (!isProduction) {
        console.log(message);
    }
};
export const console_log_message = (text, message, isProduction = false) => {
    if (!isProduction) {
        console.log(text + ' : ' + message);
    }
};

export const successAlert = (message) => {
    Alert.alert('Success', message);
};
export const errorAlert = (message) => {
    Alert.alert('Error', message);
};
export const simpleAlert = (message) => {
    Alert.alert('Alert', message);
};
export const actionAlert = (message, okayPress) => {
    Alert.alert(
        'Success',
        message,
        [
            {
                text: 'Okay',
                onPress: ()=>okayPress(),
            },
        ],
        {cancelable: false});
};


export const getLocalizedDate = (str) => {
    // var str = "2019-08-02 05:50:00";

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    // 2019-01-01T00:00:00
    // var ns = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + second

    var date = Date.UTC(year, month - 1, day, hour, minute, second);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

};
export const getLocalizedDate2 = (str) => {
    // var str = "2019-08-02 05:50:00";
    // 2019-08-03T14:24:06.000000Z

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    var date = Date.UTC(str);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

};

export const strip_html_tags = (str) => {
    if ((str === null) || (str === '')) {
        return false;
    } else {
        str = str.toString();
    }
    return str.replace(/<[^>]*>/g, '');
};


export const saveMainObject = (key, object) => {
    return new Promise((resolve, reject) => {

        AsyncStorage.setItem(key, object)
            .then(() => {
                // console.log(object)
                resolve(true);
            })
            .catch(() => {
                reject(false);
            });

    });
};
export const getSavedObject = (key) => {
    // console.log('calling')
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then(res => {
                // console.log(JSON.stringify(res))
                if (res !== null) {
                    resolve(res);
                } else {
                    // console.log("data not found")
                    resolve(false);
                }
            })
            .catch(err => {
                console.log('Error fetching saved item: ' + err);
                reject(err);
            });
    });
};

export const isNetConnected = () => {
    return new Promise((resolve, reject) => {
        NetInfo.fetch().then(state => {
            // console_log(state.isConnected);
            if (state.isConnected) {
                resolve(true);
            } else {
                errorAlert(general_network_error);
                resolve(false);
            }
        }).catch(() => {

            errorAlert(general_network_error);
            reject(false);
        });

    });
};


export const saveObject = (key, object) => {
    let str = JSON.stringify(object);
    return new Promise((resolve, reject) => {
        AsyncStorage.setItem(key, str)
            .then(() => {
                resolve(true);
            })
            .catch((err) => {
                console_log('error while saving' + err);
                reject(false);
            });

    });
};
export const getObject = (key) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then(res => {
                if (res !== null) {
                    resolve(JSON.parse(res));
                } else {
                    resolve(false);
                }
            })
            .catch(err => {
                reject(err);
            });
    });
};

export const clearAllData = () => {
    return new Promise((resolve, reject) => {

        AsyncStorage.getAllKeys()
            .then(keys => AsyncStorage.multiRemove(keys))
            .then(() => {
                // console_log('success');
                resolve()
            })
            .catch(()=>reject)
    });
};









