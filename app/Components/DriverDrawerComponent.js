import React, {Component, useEffect, useState} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import AppText from './AppText';
import {MyImage} from './MyImage';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {clearAllData} from '../Classes/auth';

export const DriverDrawerComponent = ({navigation}) => {
    const [token, setToken] = useState('');


    const closeDrawer = () => {
        navigation.closeDrawer();
    };
    const createRide = () => {
        navigation.closeDrawer();
        navigation.navigate('CreateRide');

    };
    const myRides = () => {
        navigation.closeDrawer();
        navigation.navigate('DriverMyRides');

    };
    const historyRides = () => {
        navigation.closeDrawer();
        navigation.navigate('DriverRideHistory');

    };
    const requestedRides = () => {
        navigation.closeDrawer();
        navigation.navigate('RequestRide');
    };
    const profile = () => {
        navigation.closeDrawer();
        navigation.navigate('Profile');
    };

    const logout = () => {
        navigation.closeDrawer();
        auth()
            .signOut()
            .then(() => {
                navigation.navigate('AuthStack');
                clearAllData();
            });

    };

    const MenuItem = (props) => {
        const onPress = () => {
            props.onPress();
        };
        return (
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={styles.optionItemContainer}>
                    <AppText style={styles.optionsTextStyle}>
                        {props.text}
                    </AppText>
                </View>
            </TouchableWithoutFeedback>
        );
    };

    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <View/>
                <MyImage source={require('../Asset/logo.png')}
                         imageContainerStyle={styles.headerLogoContainer}/>
                {/*<AppText>All Rights Reserved</AppText>*/}
            </View>
            <View style={styles.optionsContainer1}>
                <View style={styles.optionsContainer}>

                    <MenuItem text={'Create Rides'} onPress={createRide}/>
                    <MenuItem text={'My Rides'} onPress={myRides}/>
                    <MenuItem text={'History'} onPress={historyRides}/>
                    {/*<MenuItem text={'Requested Ride'} onPress={requestedRides}/>*/}
                    <MenuItem text={'Profile'} onPress={profile}/>
                    <MenuItem text={'Logout'} onPress={logout}/>


                </View>

                <TouchableWithoutFeedback onPress={closeDrawer}>
                    <View style={styles.closeContainer}>
                        <AppText style={[styles.optionsTextStyle, {fontSize: 15}]}>close</AppText>
                        <AppText style={styles.optionsTextStyle}> X</AppText>

                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: Colors.home_background_color,
        flex: 1,
    },
    headerContainer: {
        height: 150,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: dimen.app_padding * 2,
        backgroundColor: Colors.primary_color,
    },
    headerLogoContainer: {
        width: 80,
        height: 80,
        // margin: dimen.app_padding,
    },

    optionsContainer1: {
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        // paddingLeft: dimen.app_padding * 3,
        // backgroundColor: 'yellow'
        flex: 1,
    },
    optionsContainer: {
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingLeft: dimen.app_padding * 2,
        // backgroundColor: 'yellow'
    },
    optionItemContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 15,
        // marginBottom: 5,
    },
    optionsTextStyle: {
        fontSize: 18,
        color: Colors.normalTextColor,
        // paddingLeft: dimen.app_padding / 2,
    },
    optionsImageStyle: {
        width: 17,
        height: 17,
        // margin: dimen.app_padding,
    },
    closeContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        padding: dimen.app_padding,
        // backgroundColor: 'yellow',
    },

});

