
import {Image, Text, TouchableOpacity,StyleSheet, View} from 'react-native';
import React from 'react';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import AppText from './AppText';


export const MyImageButton = (props) => {

    return (
        <TouchableOpacity style={[{width: 60, height: 60},  props.imageContainerStyle]}
                          onPress={props.onPress}
        >
            <Image source={props.source}
                   tintColor={props.tintColor!==undefined?props.tintColor:null }
                   style={[{width: '100%', height: '100%', resizeMode: 'contain'},{tintColor:props.tintColor!==null?props.tintColor:null}, props.imageStyle]}/>
        </TouchableOpacity>
    );
};
export  const MyImage = (props) => {
    return (
        <Image source={props.source}
               tintColor={props.tintColor!==undefined?props.tintColor:null }
               style={[{width: '100%', height: '100%', resizeMode: 'contain'},{tintColor:props.tintColor!==null?props.tintColor:null}, props.imageContainerStyle]}/>
    );

};

export const CrossButton = (props) => {

    return (
        <View style={styles.crossContain}>
            <MyImageButton onPress={props.onPress}
                           source={require('../Asset/cros.png')}
                           imageContainerStyle={styles.crossButtonContainer}/>
        </View>
    );
};

export const BackButton = (props) => {

    return (
        <View style={backStyles.crossContain}>
            <MyImageButton onPress={props.onPress}
                           tintColor={Colors.black}
                           source={require('../Asset/back_arrow.png')}
                           imageContainerStyle={backStyles.crossButtonContainer}/>
        </View>
    );
};


export const HeaderTitle=(props)=>{

    const onBackPress=()=>{
        props.backPress()
    }
    return (
        <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center',marginTop:dimen.app_padding*2}}>
            <BackButton onPress={onBackPress}/>
            <AppText style={headerStyles.signUpHeader}>{props.title}</AppText>
        </View>
    )

}

const styles = StyleSheet.create({
    crossContain: {
        alignItems: 'flex-end',
        overflow:'hidden',
        marginTop:dimen.app_padding,
        marginRight:dimen.app_padding
    },
    crossButtonContainer: {
        width: 20,
        height: 20,
        // backgroundColor:Colors.primary_color,
    },
});
const backStyles = StyleSheet.create({
    crossContain: {
        // flex:1,
        justifyContent:'center',
        alignItems: 'center',
        marginRight:dimen.app_padding,

    },
    crossButtonContainer: {
        width: 20,
        height: 20,
        alignSelf:'center'
    },
});
const headerStyles = StyleSheet.create({
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
});
