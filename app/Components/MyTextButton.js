
import TextStyles from '../Styles/TextStyles';
import {ActivityIndicator, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import AppText from './AppText';
import Colors from '../Styles/Colors';


export const MyTextButton = (props) => {
    return (
        <MyTextButtonSimple onPress={props.onPress}
                            buttonText={props.buttonText}
                            buttonContainerStyle={[TextStyles.buttonStyle, props.buttonContainerStyle]}
                            buttonTextStyle={[TextStyles.buttonTextStyle, props.buttonTextStyle]}
        />

    );
};

export const MyTextButtonSimple = (props) => {
    return (
        <View>
            <TouchableOpacity style={[props.buttonContainerStyle]}
                              onPress={props.onPress}>
                <AppText style={[props.buttonTextStyle]}>{props.buttonText}</AppText>
            </TouchableOpacity>
        </View>
    );
};

export const Loader = (props) => props.loading?(
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        opacity: 0.4,
        backgroundColor: 'black',
        top: 0,
        bottom: 0,
    }}>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
):(<View>

</View>)

