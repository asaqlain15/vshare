import React, {useState, useEffect} from 'react';

import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import AppText from './AppText';
import dimen from '../Styles/Dimen';
import Colors from '../Styles/Colors';


export const DriverRideCard = (props) => {

    return (
        <TouchableOpacity onPress={()=>{props.onPress(props.data)}}>
        <View style={[styles.container,
            props.data.data.status==='upcoming'?
                {backgroundColor:Colors.primary_color}:props.data.data.status==='ongoing'?
                {backgroundColor:'#57f48f'}:{backgroundColor:'#cbcbcb'}]}>
            <View style={{}}>
                {/*<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>*/}
                {/*    <AppText style={styles.driverTextStyle}>Saqlain Abbas</AppText>*/}
                {/*    <AppText style={styles.driverTextStyle}>4.9 *</AppText>*/}
                {/*</View>*/}

                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <AppText style={[styles.driverTextStyle,{fontWeight: 'bold',fontSize:16}]}>
                        { props.data.data.status==='upcoming'?'Upcoming Ride':props.data.data.status==='ongoing'?'On Going Ride':'Completed Ride'}
                    </AppText>
                    {/*<AppText style={styles.driverTextStyle}>4.9 *</AppText>*/}
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <AppText style={styles.driverTextStyle}>{props.data.data.vehicleDetails}</AppText>
                    <AppText style={styles.driverTextStyle}>Available Seats: {props.data.data.seats}</AppText>
                </View>
            </View>
            <View style={{marginTop: 10}}>
                <AppText style={styles.locationTitleTextStyle}>
                    Pickup Location
                </AppText>
                <AppText style={styles.locationTextStyle}>
                   {props.data.data.pickupLocation}
                </AppText>
            </View>
            <View>
                <AppText style={styles.locationTitleTextStyle}>
                    Drop Off Location
                </AppText>
                <AppText style={styles.locationTextStyle}>
                    {props.data.data.dropOffLocation}
                </AppText>
            </View>
        </View>
        </TouchableOpacity>
    );
};


const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        marginBottom: dimen.app_padding,
        padding: dimen.app_padding,
    },
    driverTextStyle: {
        fontSize: 14,
    },
    locationTitleTextStyle: {
        fontSize: 11,
        color: '#606060',
    },
    locationTextStyle: {
        fontSize: 14,
        color: Colors.normalTextColor,

    },

});
