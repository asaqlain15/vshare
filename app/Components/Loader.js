import {ActivityIndicator, View, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import AppText from './AppText';
import {MyImage} from './MyImage';


 const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        opacity: 0.4,
        backgroundColor: 'black',
        top: 0,
        bottom: 0,
    }}>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
);
export const EmptyList = (props) => {


    return (

        <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center', margin: dimen.app_padding}}>
            {props.text === undefined &&
            <AppText style={{fontSize: 15, color: Colors.normalTextColor}}>No Items. Tap to reload.</AppText>}
            {props.text !== undefined &&
            <AppText style={{fontSize: 15, color: Colors.normalTextColor}}>{props.text}</AppText>}
            {props.reload !== undefined &&
            <TouchableOpacity onPress={() => props.reload()}>
                <MyImage
                    source={require('../Asset/reload.png')}
                    tintColor={Colors.primary_color}
                    imageContainerStyle={{width: 30, height: 30, marginTop: dimen.app_padding}}/>
            </TouchableOpacity>
            }
        </View>
    );
};


