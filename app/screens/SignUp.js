import React, {useState} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {CrossButton, MyImage} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import {console_log, errorAlert, saveObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';


export const SignUp = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);

    const [isRider, setIsRider] = useState(true);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');
    const [address, setAddress] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');


    const signUpPress = () => {

        if (name==='' || email===''|| phone===''|| city===''|| address===''){
            errorAlert('Please fill all required fields')
            return;
        }

        setIsLoading(true);
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
                // setIsLoading(false)
                console.log('User account created & signed in!');
                firestore()
                    .collection('Users')
                    .doc(email)
                    .set({
                        name: name,
                        phone: phone,
                        city: city,
                        address: address,
                        type: isRider ? 'rider' : 'driver',

                    })
                    .then((us) => {
                        console_log(us);
                        saveObject(Keys.user_key, {
                            email: email,
                            data: {
                                name: name,
                                phone: phone,
                                city: city,
                                address: address,
                                type: isRider ? 'rider' : 'driver',
                            },
                        });

                        if (isRider) {
                            navigation.navigate('RiderStack');
                        } else if (!isRider) {
                            navigation.navigate('DriverStack');
                        }
                    })
                    .catch(err => {
                        console_log(err);
                    });
            })
            .catch(error => {
                setIsLoading(false);
                if (error.code === 'auth/email-already-in-use') {
                    // console.log('That email address is already in use!');
                    errorAlert('That email address is already in use!');
                }

                if (error.code === 'auth/invalid-email') {
                    // console.log('That email address is invalid!');
                    errorAlert('That email address is invalid!');

                }
            });


    };


    const goBack = () => {
        navigation.pop();
    };
    const riderSelected = () => {
        setIsRider(!isRider);
    };

    return (
        <ScrollView>

            <View style={styles.mainContainer}>

                <CrossButton onPress={goBack}/>

                <AppText style={styles.signUpHeader}>Sign Up</AppText>
                <View style={{flexDirection: 'row', marginTop: 15}}>
                    <TouchableOpacity onPress={riderSelected}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View
                                style={[styles.circleStyle, isRider ? {backgroundColor: Colors.primary_color} : {backgroundColor: Colors.home_background_color}]}/>
                            <AppText style={styles.asRiderTextStyle}>As Rider</AppText>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={riderSelected}>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20}}>
                            <View
                                style={[styles.circleStyle, !isRider ? {backgroundColor: Colors.primary_color} : {backgroundColor: Colors.home_background_color}]}/>
                            <AppText style={styles.asRiderTextStyle}>As Driver</AppText>
                        </View>
                    </TouchableOpacity>

                </View>

                <AppText style={styles.inputFieldHeading}>Name</AppText>
                <TextInput onChangeText={(phone) => setName(phone)}
                           value={name}
                           placeholder={'Name'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Email Address</AppText>
                <TextInput onChangeText={(phone) => setEmail(phone)}
                           value={email}
                           placeholder={'Email'}
                           autoCapitalize='none'
                           keyboardType={'email-address'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Phone Number</AppText>
                <TextInput onChangeText={(phone) => setPhone(phone)}
                           value={phone}
                           placeholder={'phone'}
                           autoCapitalize='none'
                           keyboardType={'phone-pad'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>City</AppText>
                <TextInput onChangeText={(phone) => setCity(phone)}
                           value={city}
                           placeholder={'City'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Address</AppText>
                <TextInput onChangeText={(phone) => setAddress(phone)}
                           value={address}
                           placeholder={'Address'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Password</AppText>
                <TextInput onChangeText={(phone) => setPassword(phone)}
                           value={password}
                           placeholder={'Password'}
                           autoCapitalize='none'
                           secureTextEntry={true}
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Confirm Password</AppText>
                <TextInput onChangeText={(phone) => setConfirmPassword(phone)}
                           value={confirmPassword}
                           placeholder={'Confirm Password'}
                           autoCapitalize='none'
                           secureTextEntry={true}
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <MyTextButton
                    buttonText={'Sign Up'}
                    onPress={signUpPress}
                    buttonContainerStyle={{marginTop: 30}}
                />


                <Loader loading={isLoading}/>
            </View>
        </ScrollView>

    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start', alignItems: 'stretch',
        // paddingTop:100,
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

