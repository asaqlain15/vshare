import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {BackButton, CrossButton, HeaderTitle, MyImage, MyImageButton} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';
import {console_log, getObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';
import firestore from '@react-native-firebase/firestore';
import {Rating, AirbnbRating} from 'react-native-ratings';

export const RideDetails = ({navigation}) => {

    const [buttonText, setButtonText] = useState('Request Ride');
    const [cancleRideId, setCancelRideId] = useState('');

    const [rideDetails, setRideDetails] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [ratingDisabled, setRatingDisabled] = useState(false);


    useEffect(() => {
        let rDetail=navigation.getParam('rideDetails');
        setRideDetails(rDetail);
        getRideStatus(rDetail.id);

        checkRatingExist(rDetail);


    }, []);

    const checkRatingExist=(rdetail)=>{
        getObject(Keys.user_key)
            .then((res) => {
                firestore()
                    .collection('Users')
                    .doc(rdetail.data.email)
                    .get()
                    .then((user) => {
                        let ratingArray = user.data().rating;
                        if (ratingArray !== undefined) {
                            ratingArray.forEach(rateItem => {
                                if (rateItem.rider === res.email && rateItem.rideId === rdetail.id) {
                                    // console_log('exist rating');
                                    setRatingDisabled(true);
                                }
                            });
                        }
                    })
                    .catch(err => {
                        console_log(err);
                    });

            })
            .catch(err => console_log(err));
    }

    const getRideStatus = (rideId) => {
        setIsLoading(true);
        getObject(Keys.user_key)
            .then((res) => {
                if (!res) {
                } else {

                    firestore()
                        .collection('RideRequests')
                        .where('email', '==', res.email)
                        .get()
                        .then(querySnapshot => {
                            setIsLoading(false);
                            querySnapshot.forEach(item => {
                                if (item.data().rideId === rideId) {
                                    if (item.data().status === 'pending') {
                                        setButtonText('Cancel Ride');
                                        setCancelRideId(item.id);
                                    } else if (item.data().status === 'accept') {
                                        setButtonText('');
                                    }
                                }

                            });
                        })
                        .catch(err => {
                            console_log(err);
                            setIsLoading(false);
                        });

                }
            });
    };

    const requestRide = () => {
        setIsLoading(true);
        getObject(Keys.user_key)
            .then((res) => {
                if (!res) {
                } else {
                    firestore()
                        .collection('RideRequests')
                        // .doc(email)
                        .add({
                            email: res.email,
                            rideId: rideDetails.id,
                            status: 'pending',
                        })
                        .then(() => {
                            getRideStatus(rideDetails.id);
                            setIsLoading(false);
                        })
                        .catch(err => {
                            console_log(err);
                            setIsLoading(false);

                        });

                }
            });
    };

    const requestCancelRide = () => {
        if (buttonText === 'Request Ride') {
            requestRide();
        } else if (buttonText === 'Cancel Ride') {

            firestore()
                .collection('RideRequests')
                .doc(cancleRideId)
                .delete()
                .then((res) => {
                    setButtonText('Request Ride');
                    getRideStatus(rideDetails.id);
                }).catch(err => console_log(err));
        }
    };

    const saveRating = (rating) => {
        // console_log(rideDetails.data)
        // return;

        getObject(Keys.user_key)
            .then((res) => {

                firestore()
                    .collection('Users')
                    .doc(rideDetails.data.email)
                    .get()
                    .then((user) => {
                        // console_log(user.data().rating);

                        let ratingArray = user.data().rating;
                        if (ratingArray === undefined) {
                         ratingArray=[];
                        }
                        ratingArray.push({
                            rating: rating,
                            rideId: rideDetails.id,
                            rider: res.email,
                        });

                        let rateSum=0;
                        let count=0;
                        let avgRating=0
                        ratingArray.forEach(ratItem=>{
                            rateSum=rateSum+parseInt(ratItem.rating)
                            count=count+1;
                        })

                        if (count>0){
                            avgRating=rateSum/count;
                        }

                        // console_log(parseInt(avgRating).toString())

                        firestore()
                            .collection('Users')
                            .doc(rideDetails.data.email)
                            .update({
                                averageRating:parseInt(avgRating).toString(),
                                rating: ratingArray,
                            })
                            .then(res => {
                                // console_log(res);
                                // setRatingDisabled(true)
                                checkRatingExist(rideDetails);

                            })
                            .catch(err => console_log(err));


                        setIsLoading(false);
                    })
                    .catch(err => {
                        console_log(err);
                        setIsLoading(false);

                    });

            })
            .catch((err) => {
                console_log(err);
            });

    };

    const trackRide = () => {
        // navigation.navigate('TrackRide')

    };


    const goBack = () => {
        navigation.pop();
    };


    return (
        <View style={{flex: 1}}>
            <ScrollView style={{flex: 1, backgroundColor: Colors.home_background_color}}>
                <View style={styles.mainContainer}>
                    <HeaderTitle backPress={goBack} title={'Ride Details'}/>
                    {rideDetails !== null &&
                    <View>
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Pickup Location
                            </AppText>
                            <AppText style={styles.locationTextStyle}>
                                {rideDetails.data.pickupLocation}
                            </AppText>
                        </View>
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Drop Off Location
                            </AppText>
                            <AppText style={styles.locationTextStyle}>
                                {rideDetails.data.dropOffLocation}
                            </AppText>
                        </View>
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Date
                            </AppText>
                            <AppText style={styles.locationTextStyle}>
                                {rideDetails.data.date}
                            </AppText>
                        </View>
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Time
                            </AppText>
                            <AppText style={styles.locationTextStyle}>
                                {rideDetails.data.time}
                            </AppText>
                        </View>

                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Driver Info
                            </AppText>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    {rideDetails.driver.name}
                                </AppText>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <AppText style={styles.locationTextStyle}>
                                        {rideDetails.driver.averageRating}
                                    </AppText>
                                    <MyImage source={require('../Asset/star.png')}
                                             imageContainerStyle={{width: 20, height: 20}}
                                    />
                                </View>
                            </View>

                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    Phone
                                </AppText>
                                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <AppText style={styles.locationTextStyle}>
                                        {rideDetails.driver.phone}
                                    </AppText>
                                </View>
                            </View>
                        </View>

                        {false && <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Seats Available
                            </AppText>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    {rideDetails.data.seats}
                                </AppText>
                            </View>
                        </View>}

                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Estimated Cost
                            </AppText>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    {'\u20AC'}{rideDetails.data.cost}
                                </AppText>
                            </View>
                        </View>
                        {buttonText === '' && rideDetails.data.status !== 'complete' &&
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Ride Status
                            </AppText>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    Accepted
                                </AppText>
                            </View>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                <AppText style={styles.locationTextStyle}>
                                    Please wait for the driver to start the ride for tracking
                                </AppText>
                            </View>
                        </View>}

                        {rideDetails.data.status === 'complete' && !ratingDisabled &&
                        <View style={{marginTop: dimen.app_padding}}>
                            <AppText style={styles.locationTitleTextStyle}>
                                Rate Ride
                            </AppText>

                            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                <AirbnbRating
                                    count={5}
                                    defaultRating={0}
                                    onFinishRating={(rating) => {
                                        saveRating(rating);
                                    }}
                                    isDisabled={ratingDisabled}
                                    reviews={['Bad', 'Normal', 'Satisfying', 'Good', 'Very Good']}
                                    size={20}
                                />
                            </View>

                        </View>}
                    </View>
                    }

                    {buttonText !== '' &&
                    <MyTextButton
                        buttonText={buttonText}
                        onPress={requestCancelRide}
                        buttonContainerStyle={{marginTop: 30}}
                    />}

                    {rideDetails !== null && rideDetails.data.status === 'ongoing' &&
                    <MyTextButton
                        buttonText={'Track Ride'}
                        onPress={trackRide}
                        buttonContainerStyle={{marginTop: 30}}
                    />}


                </View>
            </ScrollView>
            <Loader loading={isLoading}/>
        </View>

    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


    locationTitleTextStyle: {
        fontSize: 13,
        color: Colors.locationTitleColor,
    },
    locationTextStyle: {
        fontSize: 17,
        color: Colors.normalTextColor,

    },


});

