import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {BackButton, CrossButton, HeaderTitle, MyImage} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';

import DateTimePicker from '@react-native-community/datetimepicker';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import {console_log, errorAlert, getObject, saveObject} from '../Classes/auth';
import firestore from '@react-native-firebase/firestore';
import {Keys} from '../Classes/Keys';


export const DriverCreateRide = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [phone, setPhone] = useState('');
    const [pickupLocation, setPickupLocation] = useState('');
    const [dropOffLocation, setDropoffLocation] = useState('');
    const [vehicleDetails, setVehicleDetails] = useState('');
    const [numberOfSeats, setNumberOfSeats] = useState('');
    const [email, setEmail] = useState('');
    const [cost, setCost] = useState('');

    useEffect(() => {
        getObject(Keys.user_key)
            .then(res => {
                if (!res) {
                } else {
                    setEmail(res.email);
                }
            });

    }, []);

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };
    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };
    const showDatepicker = () => {
        showMode('date');
    };
    const showTimepicker = () => {
        showMode('time');
    };


    const createRide = () => {
        if (pickupLocation === '' || dropOffLocation === '' || vehicleDetails === '' || numberOfSeats === '') {
            errorAlert('Please enter all fields');
            return;
        }

        setIsLoading(true);
        firestore()
            .collection('DriverRides')
            // .doc(email)
            .add({
                email: email,
                pickupLocation: pickupLocation,
                dropOffLocation: dropOffLocation,
                date: date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear(),
                time: date.getHours() + '-' + date.getMinutes(),
                seats: numberOfSeats,
                vehicleDetails: vehicleDetails,
                status: 'upcoming',
                cost: cost,

            })
            .then((us) => {
                // console_log(us);
                setIsLoading(false);
                navigation.pop();

            })
            .catch(err => {
                console_log(err);
                setIsLoading(false);

            });


    };
    const goBack = () => {
        navigation.pop();
    };


    const SelectInput = (props) => {
        return (
            <TouchableOpacity onPress={() => props.showPicker()}>
                <View style={[TextStyles.inputFieldStyle, {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingTop: 0, paddingBottom: 0,
                }]}>
                    <AppText
                        style={[{fontSize: 14, paddingTop: dimen.app_padding, paddingBottom: dimen.app_padding}]}>
                        {props.text}
                    </AppText>
                    <MyImage source={require('../Asset/dropdown.png')}
                             tintColor={Colors.primary_color}
                             imageContainerStyle={{width: 20, height: 20, marginRight: 15}}
                    />
                </View>
            </TouchableOpacity>
        );
    };

    return (

        <ScrollView style={{flex: 1, backgroundColor: Colors.home_background_color}}>

            <View style={styles.mainContainer}>

                <HeaderTitle backPress={goBack} title={'Create Ride'}/>

                <AppText style={styles.inputFieldHeading}>Pickup Location</AppText>
                <TextInput onChangeText={(phone) => setPickupLocation(phone)}
                           value={pickupLocation}
                           placeholder={'Pickup location'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Drop Off Location</AppText>
                <TextInput onChangeText={(phone) => setDropoffLocation(phone)}
                           value={dropOffLocation}
                           placeholder={'Drop Off Location'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Date</AppText>
                <SelectInput text={date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()}
                             showPicker={showDatepicker}/>

                <AppText style={styles.inputFieldHeading}>Time</AppText>
                <SelectInput text={date.getHours() + '-' + date.getMinutes()+ '-' + date.getSeconds()}
                             showPicker={showTimepicker}/>

                {show && (
                    <RNDateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        minimumDate={new Date()}
                        display="default"
                        onChange={onChange}


                    />
                )}


                <AppText style={styles.inputFieldHeading}>Vehicle Details</AppText>
                <TextInput onChangeText={(phone) => setVehicleDetails(phone)}
                           value={vehicleDetails}
                           placeholder={'Vehicle Details'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Number of seats available</AppText>
                <TextInput onChangeText={(phone) => setNumberOfSeats(phone)}
                           value={numberOfSeats}
                           placeholder={'Number'}
                           autoCapitalize='none'
                           keyboardType={'numeric'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Fuel Cost in {'\u20AC'}</AppText>
                <TextInput onChangeText={(phone) => setCost(phone)}
                           value={cost}
                           placeholder={'0.00'}
                           autoCapitalize='none'
                           keyboardType={'numeric'}
                           style={TextStyles.inputFieldStyle}/>


                <MyTextButton
                    buttonText={'Create Ride'}
                    onPress={createRide}
                    buttonContainerStyle={{marginTop: 30}}
                />


            </View>
            <Loader loading={isLoading}/>
        </ScrollView>

    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

