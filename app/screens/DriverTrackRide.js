import React, {useState, useEffect} from 'react';
import {
    Image, Text,
    StyleSheet, TouchableOpacity,
    View, PermissionsAndroid,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {HeaderTitle, MyImage, MyImageButton} from '../Components/MyImage';
import {MyTextButton} from '../Components/MyTextButton';
import {console_log, getObject} from '../Classes/auth';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {Keys} from '../Classes/Keys';
import {FlatList} from 'react-native-gesture-handler';
import {RideCard} from '../Components/RideCard';

import AppText from '../Components/AppText';
import Geolocation from '@react-native-community/geolocation';
import MapView, {Marker} from 'react-native-maps';


export const DriverTrackRide = ({navigation}) => {

    const [name, setName] = useState('');
    const [serName, setSerName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [rideId, setRideId] = useState('');

    const [locationPermission, setLocationPermission] = useState(false);
    const [shouldUpdate, setShouldUpdate] = useState('false');

    const [currentLocation, setCurrentLocation] = useState('');
    const [currentLocationLat, setCurrentLocationLat] = useState('');
    const [currentLocationLong, setCurrentLocationLong] = useState('');

    const [latlong, setLatlong] = useState({latitude: 37.78825, longitude: -122.4324});
    const [lat, setLat] = useState(navigation.getParam('latlong').latitude);
    const [lon, setLon] = useState(navigation.getParam('latlong').longitude);
    const [markersData, setMarkersData] = useState([]);
    const [markers, setMarkers] = useState([]);


    useEffect(() => {
        getCurrentLocation();
        let rid = navigation.getParam('rideId');
        setRideId(rid);

        getObject(Keys.user_key).then(res => {
            setEmail(email);

            updateMyLocation(rid, email);


        }).catch(err => console_log(err));


    }, []);


    const updateMyLocation = (rid, email) => {
        firestore()
            .collection('RideTracking')
            .doc(rid)
            .get()
            .then(ref => {
                // console_log(ref.data())
                let rideUsers = ref.data().users;
                if (rideUsers && rideUsers.length > 0) {
                    rideUsers.forEach(rUser => {
                        firestore()
                            .collection('Users')
                            .doc(rUser.email)
                            .get()
                            .then(res => {

                                let mData = {
                                    email: rUser.email,
                                    user: res.data(),
                                    lat: rUser.location[0],
                                    lon: rUser.location[1],
                                    latlng: {
                                        latitude: parseFloat(rUser.location[0]),
                                        longitude: parseFloat(rUser.location[1]),
                                    },

                                };
                                markersData.push(mData);

                                // console_log(res.data().type)
                                if (res.data().type==='driver'){
                                    setLat(parseFloat(rUser.location[0]))
                                    setLon(parseFloat(rUser.location[1]))
                                }


                                // let marker = {
                                //     title: 'VShare',
                                //     description: 'A Ride Sharing App',
                                //     latlng: {
                                //         latitude: parseFloat(rUser.location[0]),
                                //         longitude: parseFloat(rUser.location[1]),
                                //     },
                                //
                                //     // lon:rUser.location[1]
                                // };
                                // markersData.push(marker);

                                setShouldUpdate(res.id);


                            })
                            .catch(err => console_log(err));
                    });
                }
            })
            .catch(err => console_log(err));


    };

    const requestLocationPermission = () => {
        if (Platform.OS !== 'ios') {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then((status) => {
                    if (!status) {
                        PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                            {
                                title: 'Location permission',
                                message: 'VShare needs access to location services',
                                buttonNeutral: 'Ask Me Later',
                                buttonNegative: 'Cancel',
                                buttonPositive: 'OK',
                            },
                        )
                            .then((res) => {
                                if (res === 'granted') {
                                    setLocationPermission(true);
                                    getCurrentLocation();
                                }
                            })
                            .catch((err) => {
                                console_log(err);
                            });
                    } else {
                        setLocationPermission(true);
                    }
                })
                .catch((err) => {
                    console_log(err);
                });
        }
        // if (Platform.OS === 'ios') {
        //     getCurrentLocation();
        // }
    };

    const getCurrentLocation = () => {
        if (Platform.OS !== 'ios') {

            if (locationPermission) {
                // console_log('get location ')

                Geolocation.getCurrentPosition(info => {
                    if (info.coords !== undefined) {
                        console_log(info.coords);
                        setCurrentLocationLat(info.coords.latitude);
                        setCurrentLocationLong(info.coords.longitude);
                        // updateAddress(info.coords.latitude, info.coords.longitude);
                    }
                });
            } else {
                requestLocationPermission();
            }
        }
        // else {
        //     Geolocation.getCurrentPosition(info => {
        //         if (info.coords !== undefined) {
        //             // setCurrentLocationLat(info.coords.latitude);
        //             // setCurrentLocationLong(info.coords.longitude);
        //             // updateAddress(info.coords.latitude, info.coords.longitude);
        //         }
        //     });
        // }

    };


    const goBack = () => {
        navigation.pop();
    };
    return (
        <View style={{left: 0, right: 0, top: 0, bottom: 0, position: 'absolute'}} shouldUpdate={shouldUpdate}>
            <MapView
                style={{left: 0, right: 0, top: 0, bottom: 0, position: 'absolute'}}
                initialRegion={{
                    latitude: lat,
                    longitude: lon,
                    latitudeDelta: 0.0222,
                    longitudeDelta: 0.0121,
                }}

            >


                {markersData.map(marker=>{
                    console_log(marker)
                    return (
                    <Marker
                        style={{width: 40, height: 40}}
                        coordinate={marker.latlng}
                        title={marker.user.name}
                        // description={'VShare, Ride sharing app'}
                        image={marker.user.type==='driver'?require('../Asset/car_icon.png'):require('../Asset/person.png')}
                    />
                )})
                }

            </MapView>
            <View style={{padding: dimen.app_padding}}>
                <HeaderTitle backPress={goBack} title={'Track Ride'}/>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

