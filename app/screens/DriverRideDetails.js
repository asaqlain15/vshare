import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {BackButton, CrossButton, HeaderTitle, MyImage, MyImageButton} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';
import {console_log, errorAlert, getObject, saveObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';
import firestore from '@react-native-firebase/firestore';


export const DriverRideDetails = ({navigation}) => {

        const [rideData, setRideData] = useState(null);
        const [rideRequests, setRideRequests] = useState([]);
        const [isLoading, setIsLoading] = useState(false);
        const [shouldReRender, setShouldReRender] = useState('0');
        const [acceptedRequests, setAcceptedRequests] = useState(0);


        useEffect(() => {
            let rd = navigation.getParam('rideDetails');
            setRideData(rd);
            getRideRequests(rd);
        }, []);

        useEffect(() => {
            let count = 0;
            rideRequests.forEach(re => {
                if (re.rideRequest.status === 'accept') {
                    count = count + 1;
                }
            });
            setAcceptedRequests(count);
        }, [shouldReRender]);

        const getRideRequests = (rd) => {
            if (rd.data.status === 'upcoming') {
                setIsLoading(true);

                firestore()
                    .collection('RideRequests')
                    .where('rideId', '==', rd.id)
                    .get()
                    .then(querySnapshot => {
                        querySnapshot.forEach(item => {
                            firestore()
                                .collection('Users')
                                .doc(item.data().email)
                                .get()
                                .then(user => {
                                    let rReq = {id: item.id, rideRequest: item.data(), rider: user.data()};
                                    // console_log('req for'+ JSON.stringify(rReq))
                                    let contain = false;
                                    rideRequests.forEach(it => {
                                        if (it.id === item.id) {
                                            contain = true;
                                            it.rideRequest.status = item.data().status;
                                            setShouldReRender(item.id + 123);
                                        }
                                    });
                                    if (!contain) {
                                        rideRequests.push(rReq);
                                    }
                                    setShouldReRender(item.id);
                                })
                                .catch(err => {
                                    console_log(err);
                                    setIsLoading(false);
                                });
                        });

                        setIsLoading(false);
                    })
                    .catch(err => setIsLoading(false));


            }

        };

        const startRide = () => {

            getObject(Keys.current_ride_key).then(curRide=>{
                console_log(curRide);
                if (!curRide){
                    saveObject(Keys.current_ride_key, rideData.id);
                    setIsLoading(true);
                    firestore()
                        .collection('DriverRides')
                        .doc(rideData.id)
                        .update({
                            status: 'ongoing',
                        })
                        .then(() => {
                            setIsLoading(false);
                            updateRideDetails();
                        }).catch(() => {
                        setIsLoading(false);
                    });
                } else {
                    errorAlert('You can start one ride at a time. Please complete your previous ride first.')
                }
            })
        };
        const endRide = () => {
            saveObject(Keys.current_ride_key, false);
            setIsLoading(true);
            firestore()
                .collection('DriverRides')
                .doc(rideData.id)
                .update({
                    status: 'complete',
                })
                .then(() => {
                    setIsLoading(false);
                    updateRideDetails();
                }).catch(() => {
                setIsLoading(false);
            });
        };

        const trackRide=()=>{
            navigation.replace('DriverTrackRide');
        }

        const updateRideDetails = () => {
            firestore()
                .collection('DriverRides')
                .doc(rideData.id)
                .get()
                .then(item => {
                    setRideData({id: item.id, data: item.data()});
                    setIsLoading(false);
                })
                .catch(err => setIsLoading(false));

        };

        const goBack = () => {
            navigation.pop();
        };

        const updateRideStatus = (rStatus, item) => {
            setIsLoading(true);
            firestore()
                .collection('RideRequests')
                .doc(item.id)
                .update({status: rStatus})
                .then(res => {
                    getRideRequests(rideData);
                    setIsLoading(false);
                })
                .catch(err => setIsLoading(false));
        };

        const RiderRequests = (props) => {
            let requests = props.data.map((item, index) => {

                const updateRideStatus = (aStatus) => {
                    props.updateRideStatus(aStatus, item);
                };
                return (
                    <View key={index}>
                        {((props.accptedRequests != rideData.data.seats) || (props.accptedRequests == rideData.data.seats && item.rideRequest.status === 'accept')) &&
                        <View style={styles.rideRequestMainContainer}>
                            <View style={styles.rideRequestContainer}>
                                <AppText style={styles.rideRequestTextStyle}>
                                    Rider Name: {item.rider.name}
                                </AppText>

                                <AppText style={styles.rideRequestTextStyle}>
                                    Rating: {item.rider.averageRating}
                                </AppText>

                            </View>
                            <View style={styles.rideRequestContainer}>
                                <AppText style={styles.rideRequestTextStyle}>
                                    {item.rider.phone}
                                </AppText>

                                <AppText style={styles.rideRequestTextStyle}>
                                    {item.rider.city}
                                </AppText>

                            </View>
                            <View style={styles.rideRequestContainer}>
                                <AppText style={styles.rideRequestTextStyle}>
                                    Status: {item.rideRequest.status}{props.accptedRequests}
                                </AppText>
                            </View>
                            {item.rideRequest.status === 'pending' &&
                            <View style={{flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center'}}>
                                <MyTextButton
                                    buttonText={'Accept '}
                                    onPress={() => updateRideStatus('accept')}
                                    buttonContainerStyle={{marginTop: 3, alignSelf: 'center'}}
                                />
                                <MyTextButton
                                    buttonText={'Reject'}
                                    onPress={() => updateRideStatus('reject')}
                                    buttonContainerStyle={{marginTop: 3, alignSelf: 'center'}}
                                />
                            </View>}
                        </View>}
                    </View>
                );
            });

            return (
                <View>{requests}</View>
            );
        };


        if (rideData === null) {
            return <View></View>;
        } else {
            return (
                <View style={{flex: 1}}>
                    <ScrollView style={{flex: 1, backgroundColor: Colors.home_background_color}}>

                        <View style={styles.mainContainer}>

                            <HeaderTitle backPress={goBack} title={'Ride Details'}/>

                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Pickup Location
                                </AppText>
                                <AppText style={styles.locationTextStyle}>
                                    {rideData.data.pickupLocation}
                                </AppText>
                            </View>
                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Drop Off Location
                                </AppText>
                                <AppText style={styles.locationTextStyle}>
                                    {rideData.data.dropOffLocation}
                                </AppText>
                            </View>
                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Date/time
                                </AppText>
                                <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                                    <AppText style={styles.locationTextStyle}>
                                        {rideData.data.date + '      ' + rideData.data.time}
                                    </AppText>
                                </View>
                            </View>
                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Seats Available
                                </AppText>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                    <AppText style={styles.locationTextStyle}>
                                        {rideData.data.seats}
                                    </AppText>
                                </View>
                            </View>
                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Estimated Cost
                                </AppText>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                    <AppText style={styles.locationTextStyle}>
                                        {'\u20AC'}{rideData.data.cost}
                                    </AppText>
                                </View>
                            </View>
                            {rideData.status !== 'upcoming' &&
                            <View style={{marginTop: dimen.app_padding}}>
                                <AppText style={styles.locationTitleTextStyle}>
                                    Rider Requests{acceptedRequests}
                                </AppText>
                                <RiderRequests data={rideRequests}
                                               shouldReRender={shouldReRender}
                                               updateRideStatus={updateRideStatus}
                                               accptedRequests={acceptedRequests}
                                />

                            </View>}


                            {rideData.data.status === 'upcoming' &&
                            <MyTextButton
                                buttonText={'Start Ride'}
                                onPress={startRide}
                                buttonContainerStyle={{marginTop: 30}}
                            />}

                            {rideData.data.status === 'ongoing' &&
                            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                <MyTextButton
                                    buttonText={'Track Ride'}
                                    onPress={trackRide}
                                    buttonContainerStyle={{marginTop: 30}}
                                />
                                <MyTextButton
                                    buttonText={'End Ride'}
                                    onPress={endRide}
                                    buttonContainerStyle={{marginTop: 30}}
                                />
                            </View>

                            }


                        </View>
                    </ScrollView>
                    <Loader loading={isLoading}/>

                </View>
            );
        }
    }
;


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


    locationTitleTextStyle: {
        fontSize: 13,
        color: Colors.locationTitleColor,
    },
    locationTextStyle: {
        fontSize: 17,
        color: Colors.normalTextColor,

    },

    rideRequestContainer: {
        flexDirection: 'row',
        paddingTop: 3, paddingBottom: 3,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    rideRequestMainContainer: {
        borderWidth: 1,
        borderColor: Colors.primary_color,
        padding: 5, marginTop: 5,
    },

    rideRequestTextStyle: {
        fontSize: 14,
        color: Colors.normalTextColor,

    },

});

