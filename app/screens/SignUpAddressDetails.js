import React, {useState} from 'react';
import {
    Image,Text,
    StyleSheet, TouchableOpacity,
    View,
} from 'react-native';
import Colors from "../Styles/Colors";
import dimen, {Dimen} from "../Styles/Dimen";
import {TextInput} from "react-native-gesture-handler";


export const SignUpAddressDetails = ({navigation}) => {

    const [name, setName]=useState('')
    const [serName, setSerName]=useState('')
    const [username, setUsername]=useState('')
    const [email, setEmail]=useState('')

    return (
        <View style={styles.mainContainer}>

            <Text style={{fontSize:22,color:Colors.accent_color,textAlign: 'center'}}>
                Address Details
            </Text>


            <Text style={styles.titleTextStyle}>
                House number
            </Text>
            <TextInput onChangeText={(value) => setName(value)}
                       value={name}
                       placeholder={'House number'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>

            <Text style={styles.titleTextStyle}>
                Street number
            </Text>
            <TextInput onChangeText={(value) => setSerName(value)}
                       value={serName}
                       placeholder={'Street number'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>


            <Text style={styles.titleTextStyle}>
                City
            </Text>
            <TextInput onChangeText={(value) => setUsername(value)}
                       value={username}
                       placeholder={'City'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>

            <Text style={styles.titleTextStyle}>
                Country
            </Text>
            <TextInput onChangeText={(value) => setEmail(value)}
                       value={email}
                       placeholder={'Country'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>


            <TouchableOpacity onPress={()=>{ navigation.navigate('SignUpAccountDetails')}}>
                <Text style={styles.buttonTextStyle}>
                   Proceed
                </Text>
            </TouchableOpacity>


        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer:{
        flex:1,backgroundColor:Colors.primary_color,
        justifyContent:'flex-start', alignItems:'stretch',
        paddingTop:50,
        padding:dimen.app_padding
    },
    buttonTextStyle:{
        fontSize:22,
        marginTop:dimen.app_padding*4,
        margin:dimen.app_padding*2,
        textAlign:'center',
        fontWeight:'bold',
        color:Colors.primary_color,
        backgroundColor:Colors.accent_color,padding:dimen.app_padding
    },
    titleTextStyle:{
        fontSize:16,
        color:Colors.accent_color,},
    inputFieldStyle:{backgroundColor:Colors.accent_color,fontSize:16,marginTop:10},

});

