import React, {useState} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {BackButton, CrossButton, HeaderTitle, MyImage} from '../Components/MyImage';
import {FlatList} from 'react-native-gesture-handler';
import {RideCard} from '../Components/RideCard';


export const DriverRequestedRides = ({navigation}) => {

    const [phone, setPhone] = useState('');
    const [rides, setRides] = useState([{id: 1}, {id: 2}]);

    const signUpPress = () => {
        navigation.navigate('SignUp');
    };
    const goBack = () => {
        navigation.pop();
    };
    const ridePress = () => {
        navigation.navigate('RideDetails');
    };


    return (
        <View style={styles.mainContainer}>
            <HeaderTitle backPress={goBack} title={'My Rides'}/>
            <FlatList
                style={{marginTop: dimen.app_padding * 2}}
                data={rides}
                keyExtractor={(item, index) => item.id.toString()}
                numColumns={1}
                bounces={false}
                renderItem={({item}) => (
                    <RideCard data={item}
                              onPress={ridePress}
                    />
                )}
                // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                // ListEmptyComponent={
                //     <EmptyList text={'List is empty'}/>
                // }
            />
        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

