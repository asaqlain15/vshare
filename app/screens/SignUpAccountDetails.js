import React, {useState} from 'react';
import {
    Image,Text,
    StyleSheet, TouchableOpacity,
    View,
} from 'react-native';
import Colors from "../Styles/Colors";
import dimen, {Dimen} from "../Styles/Dimen";
import {TextInput} from "react-native-gesture-handler";


export const SignUpAccountDetails = ({navigation}) => {

    const [name, setName]=useState('')
    const [serName, setSerName]=useState('')
    const [username, setUsername]=useState('')
    const [email, setEmail]=useState('')

    return (
        <View style={styles.mainContainer}>

            <Text style={{fontSize:22,color:Colors.accent_color,textAlign: 'center'}}>
                Account Details
            </Text>


            <Text style={styles.titleTextStyle}>
                Bank Name
            </Text>
            <TextInput onChangeText={(value) => setName(value)}
                       value={name}
                       placeholder={'Bank Name'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>

            <Text style={styles.titleTextStyle}>
                Account Name
            </Text>
            <TextInput onChangeText={(value) => setSerName(value)}
                       value={serName}
                       placeholder={'Account Name'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>


            <Text style={styles.titleTextStyle}>
                Account Number
            </Text>
            <TextInput onChangeText={(value) => setUsername(value)}
                       value={username}
                       placeholder={'Account Number'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>

            <Text style={styles.titleTextStyle}>
                IBAN
            </Text>
            <TextInput onChangeText={(value) => setEmail(value)}
                       value={email}
                       placeholder={'IBAN'}
                       autoCapitalize='none'
                       keyboardType={'default'}
                       style={styles.inputFieldStyle}/>


            <TouchableOpacity onPress={()=>{ navigation.navigate('Home')}}>
                <Text style={styles.buttonTextStyle}>
                   Done
                </Text>
            </TouchableOpacity>


        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer:{
        flex:1,backgroundColor:Colors.primary_color,
        justifyContent:'flex-start', alignItems:'stretch',
        paddingTop:50,
        padding:dimen.app_padding
    },
    buttonTextStyle:{
        fontSize:22,
        marginTop:dimen.app_padding*4,
        margin:dimen.app_padding*2,
        textAlign:'center',
        fontWeight:'bold',
        color:Colors.primary_color,
        backgroundColor:Colors.accent_color,padding:dimen.app_padding
    },
    titleTextStyle:{
        fontSize:16,
        color:Colors.accent_color,},
    inputFieldStyle:{backgroundColor:Colors.accent_color,fontSize:16,marginTop:10},

});

