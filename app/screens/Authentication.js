import React, {useState, useEffect} from 'react';
import {
    ActivityIndicator, Alert,
    Image,
    StatusBar, TouchableOpacity, Text,
    View,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {console_log} from '../Classes/auth';


const Authentication = ({navigation}) => {

    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();

    // Handle user state changes
    function onAuthStateChanged(user) {
        // console_log(user);

        if (initializing){
            setInitializing(false)
        }
        // if (user===null)return;

        if (!user){
            navigation.navigate('AuthStack')
            // console_log('user'+user)
        } else {
            firestore()
                .collection('Users')
                .doc(user.email)
                .get()
                .then((us) => {
                    // console_log(us.data().type);
                    if (us.data().type==='rider'){
                        navigation.navigate('RiderStack');
                    }else if(us.data().type==='driver'){
                        navigation.navigate('DriverApp');
                    }
                })
                .catch(err=>{
                    console_log(err)
                })


        }

    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);


    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator/>
            <StatusBar barStyle="default"/>
        </View>
    );
};

export default Authentication;
