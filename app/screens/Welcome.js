import React, {useState} from 'react';
import {
    StyleSheet,Image,TouchableOpacity,
    View,Text,
} from 'react-native';
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";


export const Welcome = ({navigation}) => {

    return (
        <View style={{flex:1,backgroundColor:Colors.primary_color,justifyContent:'space-evenly', alignItems:'center'}}>
            <Image source={require('../Asset/logo.png')}
                   style={{width:200, height:200,}}
            />

            <TouchableOpacity onPress={()=>{ navigation.navigate('Login')}}>
                <Text style={{fontSize:22, fontWeight:'bold',
                    color:Colors.primary_color,backgroundColor:Colors.accent_color,padding:dimen.app_padding}}>
                    Get Started
                </Text>
            </TouchableOpacity>


        </View>
    );
};


const styles = StyleSheet.create({

});

