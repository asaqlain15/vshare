import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {BackButton, CrossButton, HeaderTitle, MyImage} from '../Components/MyImage';
import {FlatList} from 'react-native-gesture-handler';
import {RideCard} from '../Components/RideCard';
import {console_log, getObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';
import firestore from '@react-native-firebase/firestore';
import {Loader} from '../Components/MyTextButton';
import {EmptyList} from '../Components/Loader';


export const MyRides = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [rides, setRides] = useState([]);
    const [shouldReRender, setShouldReRender] = useState('');

    useEffect(() => {
        setIsLoading(true);
        getObject(Keys.user_key)
            .then(res => {
                if (!res) {
                    navigation.navigate('Login');
                } else {
                    firestore()
                        .collection('RideRequests')
                        .where('email', '==', res.email)
                        .get()
                        .then(querySnapshot => {
                            let ride = [];
                            let count=0
                            querySnapshot.forEach(item => {
                                count=count+1;
                                if (item.data().status === 'accept') {

                                    firestore()
                                        .collection('DriverRides')
                                        .doc(item.data().rideId)
                                        .get()
                                        .then(res => {

                                            firestore()
                                                .collection('Users')
                                                .doc(res.data().email)
                                                .get()
                                                .then(userRes => {
                                                    setIsLoading(false);
                                                    if (res.data().status !=='complete') {
                                                        let r = {id: res.id, data: res.data(), driver: userRes.data()};
                                                        rides.push(r);
                                                        setShouldReRender(res.id)

                                                    }

                                                    if (count === querySnapshot.size-1){
                                                        setIsLoading(false);
                                                    }

                                                })
                                                .catch(err => console_log(err));

                                        })
                                        .catch(err => console_log(err));
                                }
                            });
                            // setIsLoading(false);
                        });
                }
            });

    }, []);

    const goBack = () => {
        navigation.pop();
    };
    const ridePress = (item) => {
        navigation.navigate('RideDetails', {rideDetails: item});
    };


    return (
        <View style={styles.mainContainer}>
            <HeaderTitle backPress={goBack} title={'My Rides'}/>
            <FlatList
                style={{marginTop: dimen.app_padding * 2}}
                data={rides}
                extraData={shouldReRender}
                keyExtractor={(item, index) => item.id.toString()}
                numColumns={1}
                bounces={false}
                renderItem={({item}) => (
                    <RideCard data={item}
                              onPress={ridePress}
                    />
                )}
                // ListHeaderComponent={() => {return (<FlatListHeader/>)}}
                ListEmptyComponent={
                    <EmptyList text={'No ride available'}/>
                }
            />
            <Loader loading={isLoading}/>
        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

