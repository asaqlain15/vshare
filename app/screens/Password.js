import React, {useState} from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {CrossButton, MyImage} from '../Components/MyImage';
import {MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';


export const Password = ({navigation}) => {

    const [phone, setPhone] = useState('');

    const loginPress=() => {
        navigation.navigate('SignUp');
    }
    const goBack=()=>{
        navigation.pop();
    }

    return (
        <View style={styles.mainContainer}>
            <CrossButton onPress={goBack}/>
            <View style={styles.roundLogoStyle}>
                <MyImage source={require('../Asset/logo.png')}
                         imageContainerStyle={{width: 100, height: 100}}
                />
            </View>
            <AppText style={styles.enterPhoneTextStyle}>Enter your password to login</AppText>

            <TextInput onChangeText={(phone) => setPhone(phone)}
                       value={phone}
                       placeholder={'Password'}
                       autoCapitalize='none'
                       secureTextEntry={true}
                       keyboardType={'default'}
                       style={TextStyles.inputFieldStyle}/>

            <MyTextButton
                buttonText={'Login'}
                onPress={loginPress}
                buttonContainerStyle={{marginTop:30}}
            />


        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start', alignItems: 'stretch',
        // paddingTop:100,
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    enterPhoneTextStyle: {
        fontSize: dimen.normalHeadingFontSize,
        color: Colors.normalTextColor,
        marginTop: 30,

    },

});

