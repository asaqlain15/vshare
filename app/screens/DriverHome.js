import React, {useState, useEffect} from 'react';
import {
    Image, Text,
    StyleSheet, TouchableOpacity,
    View,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {HeaderTitle, MyImage, MyImageButton} from '../Components/MyImage';
import {MyTextButton} from '../Components/MyTextButton';
import {console_log, errorAlert, getObject} from '../Classes/auth';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {Keys} from '../Classes/Keys';
import AppText from '../Components/AppText';

import {PermissionsAndroid} from 'react-native';
import Geolocation from '@react-native-community/geolocation';


export const DriverHome = ({navigation}) => {


    const [availableRides, setAvailableRides] = useState('');
    const [completedRides, setCompletedRides] = useState('');
    const [user, setUser] = useState(null);
    const [locationPermission, setLocationPermission] = useState(false);

    const [currentLocationLat, setCurrentLocationLat] = useState(0.0);
    const [currentLocationLong, setCurrentLocationLong] = useState(0.0);


    useEffect(() => {
        getObject(Keys.user_key)
            .then(res => {
                // console_log('user'+JSON.stringify(res))
                setUser(res);
            });

        getObject(Keys.user_key)
            .then(res => {
                if (!res) {
                } else {
                    firestore()
                        .collection('DriverRides')
                        .where('email', '==', res.email)
                        .get()
                        .then(querySnapshot => {
                            let cRide = 0;
                            let aRide = 0;
                            querySnapshot.forEach(item => {
                                if (item.data().status !== 'complete') {
                                    aRide = aRide + 1;
                                } else {
                                    cRide = cRide + 1;
                                }
                            });
                            setAvailableRides(aRide);
                            setCompletedRides(cRide);

                        })
                        .catch(err => console_log(err));
                }
            });

        getCurrentLocation();
        requestLocationPermission();

    }, []);


    const requestLocationPermission = () => {
        if (Platform.OS !== 'ios') {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then((status) => {
                    if (!status) {
                        PermissionsAndroid.request(
                            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                            {
                                title: 'Location permission',
                                message: 'VShare needs access to location services',
                                buttonNeutral: 'Ask Me Later',
                                buttonNegative: 'Cancel',
                                buttonPositive: 'OK',
                            },
                        )
                            .then((res) => {
                                if (res === 'granted') {
                                    setLocationPermission(true);
                                    getCurrentLocation();
                                }
                            })
                            .catch((err) => {
                                console_log(err);
                            });
                    } else {
                        setLocationPermission(true);
                    }
                })
                .catch((err) => {
                    console_log(err);
                });
        }
        // if (Platform.OS === 'ios') {
        //     getCurrentLocation();
        // }
    };

    const getCurrentLocation = () => {
        if (Platform.OS !== 'ios') {

            if (locationPermission) {
                Geolocation.getCurrentPosition(info => {
                    if (info.coords !== undefined) {
                        console_log(info.coords);
                        setCurrentLocationLat(info.coords.latitude);
                        setCurrentLocationLong(info.coords.longitude);
                        // updateAddress(info.coords.latitude, info.coords.longitude);
                    }
                });
            } else {
                requestLocationPermission();
            }
        }

    };


    const openDrawer = () => {
        navigation.openDrawer();
    };
    const createRide = () => {
        navigation.navigate('CreateRide');
    };
    const trackRide = () => {
        getObject(Keys.current_ride_key)
            .then(curRide => {
                console_log(curRide);
                if (!curRide) {
                    errorAlert('No Active Ride Available');
                } else {
                    // console_log(currentLocationLat)
                    // return
                    navigation.navigate('DriverTrackRide', {
                        rideId: curRide,
                        latlong: {latitude: currentLocationLat, longitude: currentLocationLong},
                    });
                }

            })
            .catch(err => console_log(err));
    };

    return (
        <View style={styles.mainContainer}>

            <MyImageButton
                onPress={openDrawer}
                source={require('../Asset/menu.png')}
                tintColor={Colors.primary_color}
                imageContainerStyle={{width: 25, height: 25, position: 'absolute', top: 30, left: 20}}
            />

            <AppText style={styles.headerStyle}>Driver Dashboard</AppText>

            {user !== null &&
            <View style={{
                margin: dimen.app_padding,
                padding: dimen.app_padding,
                alignItems: 'center',
                marginTop: dimen.app_padding * 2,
                backgroundColor: Colors.primary_color,
            }}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={styles.nameTextStyle}>
                        Name:
                    </Text>
                    <Text style={[styles.nameTextStyle, {marginLeft: dimen.app_padding}]}>
                        {user.data.name}
                    </Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: dimen.app_padding}}>
                    <Text style={styles.nameTextStyle}>
                        Phone:
                    </Text>
                    <Text style={[styles.nameTextStyle, {marginLeft: dimen.app_padding}]}>
                        {user.data.phone}
                    </Text>
                </View>
            </View>}


            <View style={{margin: dimen.app_padding, alignItems: 'center', marginTop: dimen.app_padding * 2}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={styles.buttonTextStyle}>
                        Total Completed Rides:
                    </Text>
                    <Text style={[styles.buttonTextStyle, {marginLeft: dimen.app_padding}]}>
                        {completedRides}
                    </Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: dimen.app_padding}}>
                    <Text style={styles.buttonTextStyle}>
                        Total Available Rides:
                    </Text>
                    <Text style={[styles.buttonTextStyle, {marginLeft: dimen.app_padding}]}>
                        {availableRides}
                    </Text>
                </View>
            </View>


            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                margin: dimen.app_padding,
            }}>
                <MyTextButton
                    buttonText={'Create Ride'}
                    onPress={createRide}
                    buttonContainerStyle={{margin: 0}}
                />
                <MyTextButton
                    buttonText={'Track Current Ride'}
                    onPress={trackRide}
                    buttonContainerStyle={{margin: 0}}
                />
            </View>


        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start', alignItems: 'stretch',
        paddingTop: 50,
        // padding:dimen.app_padding
    },
    buttonTextStyle: {
        fontSize: 16,
        // marginTop: dimen.app_padding,
        // margin: dimen.app_padding,
        // textAlign:'center',
        fontWeight: 'bold',
        color: Colors.normalTextColor,
        backgroundColor: Colors.accent_color,
    },
    nameTextStyle: {
        fontSize: 16,
        // marginTop: dimen.app_padding,
        // margin: dimen.app_padding,
        // textAlign:'center',
        fontWeight: 'bold',
        color: Colors.button_text_color,
        // backgroundColor: Colors.accent_color,
    },
    titleTextStyle: {
        fontSize: 16,
        color: Colors.accent_color,
    },
    inputFieldStyle: {backgroundColor: Colors.accent_color, fontSize: 16, marginTop: 10},
    headerStyle: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },

});

