import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View, TouchableOpacity, ScrollView,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {CrossButton, MyImage} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';
import {console_log, errorAlert, getObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';
import firestore from '@react-native-firebase/firestore';


export const Profile = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');
    const [address, setAddress] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');


    useEffect(() => {
        getObject(Keys.user_key)
            .then((res) => {
                console.log(res);
                setName(res.data.name);
                setEmail(res.email);
                setPhone(res.data.phone);
                setCity(res.data.city);
                setAddress(res.data.address);
            });

    }, []);

    const updatePress = () => {
        if (name === '' || phone === '' || city === '' || address === '') {
            errorAlert('Please fill all required fields');
            return;
        }

        setIsLoading(true);
        firestore()
            .collection('Users')
            .doc(email)
            .update({
                name: name,
                phone: phone,
                city: city,
                address: address,
            })
            .then((res) => {
                console_log(res);
                setIsLoading(false);
            })
            .catch((err) => {
                setIsLoading(false);
                console_log(err);
            });
    };
    const goBack = () => {
        navigation.pop();
    };

    return (
        <View style={{flex:1,backgroundColor:Colors.home_background_color}}>
        <ScrollView style={{flex: 1}}>
            <View style={styles.mainContainer}>

                <CrossButton onPress={goBack}/>

                <AppText style={styles.signUpHeader}>Profile</AppText>


                <AppText style={styles.inputFieldHeading}>Name</AppText>
                <TextInput onChangeText={(phone) => setName(phone)}
                           value={name}
                           placeholder={'Name'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                <AppText style={styles.inputFieldHeading}>Email Address</AppText>
                <TextInput onChangeText={(phone) => setEmail(phone)}
                           value={email}
                           placeholder={'Email'}
                           autoCapitalize='none'
                           editable={false}
                           keyboardType={'email-address'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Phone Number</AppText>
                <TextInput onChangeText={(phone) => setPhone(phone)}
                           value={phone}
                           placeholder={'phone'}
                           autoCapitalize='none'
                           keyboardType={'phone-pad'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>City</AppText>
                <TextInput onChangeText={(phone) => setCity(phone)}
                           value={city}
                           placeholder={'City'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Address</AppText>
                <TextInput onChangeText={(phone) => setAddress(phone)}
                           value={address}
                           placeholder={'Address'}
                           autoCapitalize='none'
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>


                {false && <View>
                <AppText style={styles.inputFieldHeading}>Password</AppText>
                <TextInput onChangeText={(phone) => setPassword(phone)}
                           value={password}
                           placeholder={'Password'}
                           autoCapitalize='none'
                           secureTextEntry={true}
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>

                <AppText style={styles.inputFieldHeading}>Confirm Password</AppText>
                <TextInput onChangeText={(phone) => setConfirmPassword(phone)}
                           value={confirmPassword}
                           placeholder={'Confirm Password'}
                           autoCapitalize='none'
                           secureTextEntry={true}
                           keyboardType={'default'}
                           style={TextStyles.inputFieldStyle}/>
                </View>}


                <MyTextButton
                    buttonText={'Update'}
                    onPress={updatePress}
                    buttonContainerStyle={{marginTop: 30}}
                />


            </View>
        </ScrollView>
        <Loader loading={isLoading}/>
        </View>


);
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start', alignItems: 'stretch',
        // paddingTop:100,
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 30,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    signUpHeader: {
        fontSize: 30,
        color: Colors.primary_color,
        alignSelf: 'center',
        fontWeight: 'bold',
        // marginTop: 10,

    },
    circleStyle: {
        width: 26, height: 26,
        borderRadius: 13, borderWidth: 1,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.primary_color,
    },
    asRiderTextStyle: {
        fontSize: 17, marginLeft: 10,
    },

    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },


});

