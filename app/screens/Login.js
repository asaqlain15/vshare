import React, {useState,useEffect} from 'react';
import {
    ScrollView,
    StyleSheet, TouchableOpacity,
    View,
} from 'react-native';
import Colors from '../Styles/Colors';
import dimen from '../Styles/Dimen';
import {TextInput} from 'react-native-gesture-handler';
import {MyImage} from '../Components/MyImage';
import {Loader, MyTextButton} from '../Components/MyTextButton';
import TextStyles from '../Styles/TextStyles';
import AppText from '../Components/AppText';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {console_log, errorAlert, saveObject} from '../Classes/auth';
import {Keys} from '../Classes/Keys';


export const Login = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    async function bootstrap() {
        console.log('settings');
        await firestore().settings({
            persistence: false, // disable offline persistence
        });
    }

    const loginPress =  () => {
        setIsLoading(true)
        auth()
            .signInWithEmailAndPassword(email, password)
            .then((res) => {
                firestore()
                    .collection('Users')
                    .doc(email)
                    .get()
                    .then((us) => {
                        setIsLoading(false)
                        saveObject(Keys.user_key,{email:email,data:us.data()})
                        if (us.data().type==='rider'){
                            navigation.navigate('RiderStack');
                        }else if(us.data().type==='driver'){
                            navigation.navigate('DriverApp');
                        }
                    })
                    .catch(err=>{
                        setIsLoading(false)
                        console_log(err)
                    })
            })
            .catch(error => {
                setIsLoading(false)
                if (error.code === 'auth/email-already-in-use') {
                    errorAlert('That email address is already in use!')
                }
                if (error.code === 'auth/invalid-email') {
                    errorAlert('That email address is invalid!');

                }
            });

    };

    // Handle user state changes
    function onAuthStateChanged(user) {
        // console_log('auth changed: '+user)

    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);




    return (
        <View style={styles.mainContainer}>

            <View style={styles.roundLogoStyle}>
                <MyImage source={require('../Asset/logo.png')}
                         imageContainerStyle={{width: 100, height: 100}}
                />
            </View>
            <AppText style={styles.enterPhoneTextStyle}>
                Login to your account
            </AppText>


            <AppText style={styles.inputFieldHeading}>Email</AppText>
            <TextInput onChangeText={(email) => setEmail(email)}
                       value={email}
                       placeholder={'Email'}
                       autoCapitalize='none'
                       keyboardType={'email-address'}
                       style={TextStyles.inputFieldStyle}/>

            <AppText style={styles.inputFieldHeading}>Password</AppText>
            <TextInput onChangeText={(password) => setPassword(password)}
                       value={password}
                       placeholder={'Password'}
                       autoCapitalize='none'
                       secureTextEntry={true}
                       keyboardType={'default'}
                       style={TextStyles.inputFieldStyle}/>

            <MyTextButton
                buttonText={'Login'}
                onPress={loginPress}
                buttonContainerStyle={{marginTop: 30}}
            />

            <TouchableOpacity onPress={()=>navigation.navigate('SignUp')}>
                <AppText style={styles.enterPhoneTextStyle}>
                    Don't have account? Sign Up
                </AppText>
            </TouchableOpacity>

            <Loader loading={isLoading}/>

        </View>
    );
};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: Colors.home_background_color,
        justifyContent: 'flex-start', alignItems: 'stretch',
        // paddingTop:100,
        padding: dimen.app_padding,
    },
    buttonTextStyle: {
        fontSize: 22,
        marginTop: dimen.app_padding * 4,
        margin: dimen.app_padding * 2,
        textAlign: 'center',
        fontWeight: 'bold',
        color: Colors.primary_color,
        backgroundColor: Colors.accent_color, padding: dimen.app_padding,
    },
    roundLogoStyle: {
        backgroundColor: Colors.primary_color,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50, overflow: 'hidden',
        alignSelf: 'center',
        marginTop: 50,
    },
    inputFieldStyle: {
        backgroundColor: Colors.accent_color,
        fontSize: 14,
        marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
    },
    enterPhoneTextStyle: {
        fontSize: dimen.normalHeadingFontSize,
        color: Colors.normalTextColor,
        marginTop: 30,
        marginBottom: 10,

    },
    inputFieldHeading: {
        fontSize: 16,
        marginBottom: 5,
        marginTop: 10,
    },

});

