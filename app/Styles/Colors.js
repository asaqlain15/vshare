const Colors = {

    primary_color: '#F78B8B',
    accent_color:'#ffffff',

    button_text_color:'#ffffff',
    home_background_color:'#ffffff',
    normalTextColor: '#363636',
    locationTitleColor: '#606060',


    black: '#000000',
    night: '#333333',
    charcoal: '#474747',
    gray: '#7D7D7D',
    lightishgray: '#9D9D9D',
    lightgray: '#D6D6D6',
    smoke: '#EEEEEE',
    white: '#FFFFFF',
    ypsDark: '#47546E',
    yps: '#637599',
    ypsLight: '#7B92BF',
    cosmic: '#963D32',
    transparent:'rgba(52, 52, 52, 0.8)'

};
export default Colors;
