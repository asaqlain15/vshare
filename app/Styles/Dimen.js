import {Dimensions} from 'react-native';
export const deviceWidth = Dimensions.get('window').width;
export const deviceHeight = Dimensions.get('window').height;
const dimen={
    small_padding:10,
    app_padding:15,
    login_border_radius:25,
    login_input_margin:10,
    login_input_text_font:14,
    buttonFontSize:16,
    normalHeadingFontSize:16,
    textInputFontSize:14,
    home_item_padding:5,

    bottom_user_padding:30,
    bottom_margin_for_bottom_menu:56,
    bottom_tab_height:66,
    border_radius:10,

    carouselContainerWidth:deviceWidth,


}



export default dimen;
