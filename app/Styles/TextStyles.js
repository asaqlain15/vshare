import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View} from 'react-native';
import Colors from './Colors';
import dimen from './Dimen';

const TextStyles = StyleSheet.create({
    inputstyle: {
        height: 50,
        // margin:dimen.small_padding,
        padding: 15,
        borderRadius: 10,
        borderColor: Colors.border_color,
        backgroundColor: Colors.home_widget_background_color,
        borderWidth: 1,
        fontSize: dimen.textInputFontSize,
    },
    stericStyle: {color: 'red', marginLeft: 5},
    stericContainer: {flexDirection: 'row'},
    settingHeadingStyle: {
        fontFamily: 'roboto-bold',
        marginLeft: dimen.small_padding,
        fontSize: dimen.textInputFontSize,
    },

    buttonStyle: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        backgroundColor: Colors.primary_color,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 18,
        paddingRight: 18,
        // borderRadius: dimen.login_border_radius,
        borderTopLeftRadius:10,

    },
    allTripsHeaderDot: {
        width: 8,
        height: 8,
        // padding: 1,
        borderRadius: 4,
        position: 'absolute',
        resizeMode: 'contain',
        backgroundColor: 'red',
        top: 15,
        left: 15,
        // left: 0,
        // bottom: 0,
    },
    smallButtonView: {
        borderRadius: 15,
        borderWidth: 2,
        borderColor: Colors.primary_color,
        backgroundColor: Colors.home_widget_background_color,
    },
    smallButtonText: {
        fontSize: 11,
        fontFamily: 'roboto-bold',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 2,
        paddingBottom: 2,
    },
    buttonTextStyle: {
        color: Colors.button_text_color,
        fontSize: dimen.buttonFontSize,
    },
    inputFieldStyle:Platform.OS!=='ios'? {
        backgroundColor: Colors.accent_color,
        fontSize: dimen.login_input_text_font,
        // marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        paddingLeft:dimen.app_padding
    }:{
        backgroundColor: Colors.accent_color,
        fontSize: dimen.login_input_text_font,
        // marginTop: dimen.app_padding,
        borderColor: Colors.primary_color,
        borderWidth: 1,
        padding:dimen.app_padding
    },

    inputField: Platform.OS==='ios'? {
        margin: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow: 1,

    }:{
        // margin: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginLeft: dimen.app_padding - 4,
        fontSize: 13,
        flexGrow: 1,

    },

});


export default TextStyles;
