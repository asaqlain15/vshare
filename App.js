import React, {useState} from 'react';
import {
    SafeAreaView,
    StatusBar, Platform,
} from 'react-native';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import SplashScreen from 'react-native-splash-screen';
import Colors from './app/Styles/Colors';
import {Login} from './app/screens/Login';
import {Welcome} from './app/screens/Welcome';
import {SignUp} from './app/screens/SignUp';
import {SignUpAccountDetails} from './app/screens/SignUpAccountDetails';
import {SignUpAddressDetails} from './app/screens/SignUpAddressDetails';
import {Home} from './app/screens/Home';
import {Password} from './app/screens/Password';
import {DrawerComponent} from './app/Components/DrawerComponent';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {Profile} from './app/screens/Profile';
import {RequestRide} from './app/screens/RequestRide';
import {ExploreRides} from './app/screens/ExploreRides';
import {RideDetails} from './app/screens/RideDetails';
import {MyRides} from './app/screens/MyRides';
import crashlytics from '@react-native-firebase/crashlytics';
import Authentication from './app/screens/Authentication';
import {DriverHome} from './app/screens/DriverHome';
import {DriverDrawerComponent} from './app/Components/DriverDrawerComponent';
import {DriverCreateRide} from './app/screens/DriverCreateRide';
import {DriverMyRides} from './app/screens/DriverMyRides';
import {DriverRequestedRides} from './app/screens/DriverRequestedRides';
import {DriverRideDetails} from './app/screens/DriverRideDetails';
import {DriverTrackRide} from './app/screens/DriverTrackRide';
import {DriverRideHistory} from './app/screens/DriverRideHistory';
import {HistoryRides} from './app/screens/History';


class App extends React.Component {
    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <StatusBar
                    backgroundColor={Colors.primary_color}
                    translucent={false}
                    barStyle="dark-content"
                    animated
                    hidden={true}/>

                <AppContainer/>
            </SafeAreaView>
        );
    }
};

const RiderStack = createStackNavigator(
    {
        Home: {
            screen: Home,
        },
        Profile: {
            screen: Profile,
        },
        RequestRide: {
            screen: RequestRide,
        },
        ExploreRides: {
            screen: ExploreRides,
        },
        History: {
            screen: HistoryRides,
        },
        RideDetails: {
            screen: RideDetails,
        },
        MyRides: {
            screen: MyRides,
        },
        DriverTrackRide: {
            screen: DriverTrackRide,
        },

    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
    },
);
const AuthStack = createStackNavigator(
    {
        Login: {
            screen: Login,
        },
        Password: {
            screen: Password,
        },
        SignUp: {
            screen: SignUp,
        },
        SignUpAccountDetails: {
            screen: SignUpAccountDetails,
        },
        SignUpAddressDetails: {
            screen: SignUpAddressDetails,
        },
        Home: {
            screen: Home,
        },

    },
    {
        initialRouteName: 'Login',
        headerMode: 'none',
    },
);

const DrawerNavigator = createDrawerNavigator({
        RiderStack: {
            screen: RiderStack,
        },

    },
    {
        initialRouteName: 'RiderStack',
        contentComponent: DrawerComponent,
        drawerBackgroundColor: 'transparent',
    },
);


const DriverStack = createStackNavigator(
    {
        Home: {
            screen: DriverHome,
        },
        Profile: {
            screen: Profile,
        },
        CreateRide: {
            screen: DriverCreateRide,
        },
        DriverMyRides: {
            screen: DriverMyRides,
        },
        DriverRideHistory: {
            screen: DriverRideHistory,
        },
        DriverRequestedRides: {
            screen: DriverRequestedRides,
        },
        DriverRideDetails: {
            screen: DriverRideDetails,
        },

        DriverTrackRide: {
            screen: DriverTrackRide,
        },

    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
    },
);
const DrawerNavigatorDriver = createDrawerNavigator({
        DriverStack: {
            screen: DriverStack,
        },

    },
    {
        initialRouteName: 'DriverStack',
        contentComponent: DriverDrawerComponent,
        drawerBackgroundColor: 'transparent',
    },
);


const SwitchNavigator = createSwitchNavigator(
    {
        Authentications: Authentication,
        AuthStack: AuthStack,
        RiderApp: DrawerNavigator,
        DriverApp: DrawerNavigatorDriver,
    },
    {
        initialRouteName: 'Authentications',
    },
);


const AppContainer = createAppContainer(SwitchNavigator);
export default App;
